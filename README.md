# Deployment identifier status

This is a tiny helper module to add [deployment identifier] status to the
Drupal's status report. Status report will show if deployment identifier is
set and what value it has.

## Requirements

Drupal core only.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


[deployment identifier]: https://git.drupalcode.org/project/drupal/-/blob/81e9500d67f5af084b2f34a84fff9acc94d94944/sites/default/default.settings.php#L287-295
